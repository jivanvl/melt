# Melt README

This is a Ruby on Rails app for a point of sale system, kinda like shopify but way tinier.

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

  * Make sure to install [lefthook](https://github.com/evilmartians/lefthook/blob/master/docs/ruby.md)

    ```ruby
      gem install lefthook
    ``` 

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
